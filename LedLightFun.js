var five = require("johnny-five");
var board = new five.Board();

board.on('ready', function(){
		var light = new five.Pin(7);
		var led = new five.Led(7);
		var counter =0;


var LedLight = function(){
	this.on = function(){
		led.on();
	}
	this.off = function(){
		led.off();
	}
	this.blink = function(){
		setInterval(function(){
			led.on();
		},5000)

		setInterval(function(){
			led.off();
		},7000)
	}
}
var light = new LedLight(7);
light.on();
light.off();
light.blink();

setInterval(function(){
	counter++;
	console.log(counter);
	if (counter>=5 && counter<10) {
		light.on();
	}
	else if (counter>=10 && counter<15) {
		light.blink();
	}
	else if (counter===15) {
		counter=0;
		light.off();
	}
},2000)

	});
